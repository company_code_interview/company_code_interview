# Use an official Python runtime as a parent image
FROM python:3.8-slim

WORKDIR /usr/src/app

# Copy the current directory contents into the container at /usr/src/app
COPY . .

# Install any needed packages specified in requirements.txt
RUN pip install requests
RUN apt-get update && apt-get -y install cron

# Give execution rights on the cron job
RUN touch /etc/cron.d/
RUN chmod 0644 /etc/cron.d

# Apply cron job
COPY crontab /etc/cron.d/

# Create the log file to be able to run tail
RUN touch /var/log/cron.log

# Run the command on container startup
CMD cron && tail -f /var/log/cron.log