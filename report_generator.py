import requests
import json
from datetime import datetime
import os

# Function to fetch the top 10 downloaded models from Hugging Face
def fetch_top_models():
    response = requests.get("https://huggingface.co/api/models?sort=downloads" , verify=False)
    models = response.json()
    top_models = models[:10]
    return top_models

# Function to generate a report
def generate_report(top_models):
    report = f"Top 10 Downloaded Models Report - {datetime.now().strftime('%Y-%m-%d %H:%M:%S')}\n"
    report += "\n".join([f"{model['modelId']} - Downloads: {model['downloads']}" for model in top_models])
    with open("report.txt", "w") as file:
        file.write(report)

def stop_container():
    os.system("kill 1")

def main():
    top_models = fetch_top_models()
    generate_report(top_models)
    stop_container()

if _name_ == "_main_":
    main()